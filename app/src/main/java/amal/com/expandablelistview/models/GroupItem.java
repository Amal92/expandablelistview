package amal.com.expandablelistview.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/1/2015.
 */
public class GroupItem {
    private String title;
    private List<ChildItem> items = new ArrayList<ChildItem>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ChildItem> getItems() {
        return items;
    }

    public void setItems(List<ChildItem> items) {
        this.items = items;
    }
}
