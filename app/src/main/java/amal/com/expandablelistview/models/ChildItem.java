package amal.com.expandablelistview.models;

/**
 * Created by user on 10/1/2015.
 */
public class ChildItem {
   private String name;
   private String picture;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
